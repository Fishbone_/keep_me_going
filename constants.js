const words = {
    starters : ['keep going', 'move on', 'go on', 'carry on', 'hang on', 'keep up'],
    pronouns : ['mate', 'developer', 'software enthusiast', 'fellow',],
    doSyns : ['do it', 'make it', 'get there', 'rack up'],
    phrases : ['for the education', 'for building the best web app', 'for the money', 'for the success', 'to build a better world', 'to make your life easier', 'for new job opportunities', 'to change the world', 'to solve problems', 'to get the very best, like no one ever was', 'to get the next satoshi']
}
module.exports = {words};
   