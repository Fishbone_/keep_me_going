# Keep me going - motivational messages
A random motivational mixed message generator to keep you going in Programming written in Node.js.
You can select either personal messages, asking for your name or generalized messages.
Also you can choose between displaying the message as ASCII Art or just as normal text.
## Used Libraries
* ascii-art.js
## Features
* Motivational random messages to keep you going
* Custom keyword to who the message is to e.g. your name
* ASCII motivation message mode
# Usage
1.Install Node.js on your machine - see [Node.js page](https://nodejs.org/en/ "Node.js download page") for how to install\
2.Start the program\
`node keepMeGoing.js`\
![generalized motivational message](images/general.png "generalized Message")\
![personalized motivational message](images/personalized.png "personalized message")\
3.For ascii mode use\
`node keepMeGoing.js ascii`\
![generalized motivational ASCII message](/images/general-ascii.png "generalized ASCII Message")\
![personalized motivational ASCII message](images/personalized-ascii.png "personalized ASCII message")
# Inspiration
This project was inspired by the mixed words portfolio project at codecademys Full Stack Engineer path