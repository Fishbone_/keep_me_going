const { exit } = require('process');
const readline = require('readline');
const asciiArt = require('ascii-art');
const { executionAsyncId } = require('async_hooks');
const { isError } = require('util');
const {stdin: input, stdout: output} = process; 
const constants = require('./constants.js')

const rl = readline.createInterface({input, output});

const keepGoing = {
    _asciiMode : false,
    _choice : undefined,
    _getRandomFromArr(arr){
        return arr[Math.floor(Math.random() * arr.length)];
    },
    detectMode(personalization=undefined){
        if (process.argv[2] === 'ascii'){
            this._asciiMode = true;
        }
        this.buildPhrase(this._asciiMode ? this.printASCII : this.printPhrase, personalization);
    },
    printPhrase(part1, part2){
        console.log(`${part1} ${part2}`);
        exit(1);
    },
    printASCII(part1, part2){
        asciiArt.font(part1, 'Doom', (err, rendered)=>{
            console.log(err ? 'A error occured' : rendered);
            asciiArt.font(part2, 'Doom', (err, rendered)=>{
                console.log(err ? 'A error occured' : rendered);
                process.exit(1);
            });
        });
    },
    buildPhrase(callback, personalization=undefined){
        let pronoun;
        if(personalization === undefined || typeof personalization !== 'string'){
                pronoun = this._getRandomFromArr(constants.words.pronouns);
            }
        else{
                pronoun = personalization;
            }
        const motiv = `${this._getRandomFromArr(constants.words.starters)} ${pronoun} ${this._getRandomFromArr(constants.words.doSyns)}`;
        const motiv2 = `${this._getRandomFromArr(constants.words.phrases)}!`;
        callback(motiv, motiv2); 
    },
    getMotivation(){
        rl.question(`Welcome to keep me going! What kind of motivation do you want to get?\n 1. Get personalized motivation\n 2. Get general motivation\n 3. To quit keepMeGoing\n`, (input) => {
            this._choice = input;
            switch (this._choice){
                case '1':
                    rl.question('Please enter your name:', (input) => {
                        let personalization = input;
                        this.detectMode(personalization);
                    });
                    break;
                case '2':
                    this.detectMode();
                    break;
                case '3':
                    process.exit(1);
                default:
                    console.log('Unknown choice. Select 1, 2 or 3');
                    this.getMotivation();
                    break;
            };
        });
    }
};
keepGoing.getMotivation();